<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/authors', 'AuthorController@index'); //listado
$router->post('/authors', 'AuthorController@store'); //crear
$router->get('/authors/{author}', 'AuthorController@show');  //detalle
$router->put('/authors/{author}', 'AuthorController@update'); //editar
$router->delete('/authors/{author}', 'AuthorController@destroy'); //eliminar



/*
list
create
edith

*/